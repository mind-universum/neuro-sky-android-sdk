### CONFIGURATION ==================================================================================
image: universumstudios/android:api-27

# Clone only in depth necessary to build the latest code base.
clone:
  depth: 1

### PIPELINES ======================================================================================
pipelines:
  # Pipeline which runs by default for all branches (except those configured below).
  default:
    - step:
        caches:
          - maven
          - gradle
        script:
          # Grant permissions to CI scripts.
          - ./ci/permissions.sh
          # Build code base for the Development environment.
          - ./ci/build.sh
          # Run tests.
          - ./ci/test.sh
  # Pipelines which run only for a specific branches.
  branches:
    '*release/*':
      - step:
          caches:
            - maven
            - gradle
          script:
            # Grant permissions to CI scripts.
            - ./ci/permissions.sh
            # Build code base for the Production environment.
            - ./ci/build.sh
            # Run tests.
            - ./ci/test.sh
            # Deploy release artifacts.
            - ./ci/deploy.sh
    '*master*':
      # Do not run for master branch in order to spare build minutes of the CI server.
            - step:
                script:
                  - echo "On master branch. Ignoring CI build."