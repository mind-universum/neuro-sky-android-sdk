/*
 * =================================================================================================
 *                             Copyright (C) 2017 Martin Albedinsky
 * =================================================================================================
 *         Licensed under the Apache License, Version 2.0 or later (further "License" only).
 * -------------------------------------------------------------------------------------------------
 * You may use this file only in compliance with the License. More details and copy of this License
 * you may obtain at
 *
 * 		http://www.apache.org/licenses/LICENSE-2.0
 *
 * You can redistribute, modify or publish any part of the code written within this file but as it
 * is described in the License, the software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES or CONDITIONS OF ANY KIND.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * =================================================================================================
 */
package universum.studios.android.test.instrumented

import android.app.Instrumentation
import android.content.Context
import android.support.annotation.CallSuper
import android.support.annotation.NonNull
import android.support.annotation.WorkerThread
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith

/**
 * Class that may be used to group suite of **Android instrumented tests**.
 *
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4::class)
abstract class InstrumentedTestCase {

    /**
     */
    companion object {

        /**
         * Delegates to [Instrumentation.waitForIdleSync].
         */
        @JvmStatic @WorkerThread protected fun waitForIdleSync() {
            InstrumentationRegistry.getInstrumentation().waitForIdleSync()
        }
    }

    /**
     * Target context obtained from the [InstrumentationRegistry].
     *
     * It is always valid between calls to [.beforeTest] and [.afterTest].
     *
     * @see InstrumentationRegistry.getTargetContext
     */
    @NonNull protected lateinit var context: Context

    /**
     * Called before execution of each test method starts.
     */
    @Before @CallSuper @Throws(Exception::class) fun beforeTest() {
        // Inheritance hierarchies may for example acquire here resources needed for each test.
        this.context = InstrumentationRegistry.getTargetContext()
    }

    /**
     * Called after execution of each test method finishes.
     */
    @After @CallSuper @Throws(Exception::class) fun afterTest() {
        // Inheritance hierarchies may for example release here resources acquired in beforeTest() call.
    }
}