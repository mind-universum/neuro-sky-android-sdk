#!/usr/bin/env bash

chmod +x ./ci/checks.sh
chmod +x ./ci/download-keystore.sh
chmod +x ./ci/increment-version-code.sh
chmod +x ./ci/build.sh
chmod +x ./ci/test.sh
chmod +x ./gradlew
